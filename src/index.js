import * as PIXI from 'pixi.js';
import * as Player from './player.js';
import * as keyboard from 'pixi.js-keyboard';

const app = new PIXI.Application({
    width: 800, height: 600, backgroundColor: 0x000000,
});
document.body.appendChild(app.view);

const container = new PIXI.Container();

app.stage.addChild(container);

// Create a new texture

// Create a new texture

var x=0;
var y=0;


let settings = {
    SHOW_DEBUG : true
}


// load the texture we need
app.loader.add('background', 'assets/blue.png')
    .add('player', 'assets/playerShip1_orange.png')
    .add('enemy_a', 'assets/playerShip1_orange.png')
    .add('laserino', 'assets/laser.png')
    .add('enemy_b', 'assets/playerShip1_orange.png');

app.loader.load(init);


function init(loader, resources) {
    let score = 0;
    //let enemy = createEnemy();

    let bullets = Array();
    let enemies = Array();


    // enemy.position.set(160, 100);
    // enemy._debug = {
    //     color : 0xff2222,
    //     shape : "circle"
    // };

    
    // This creates a background
    const background = new PIXI.TilingSprite(
        resources.background.texture,
        app.renderer.width/2,
        app.renderer.height*1.1,
    );

    const player = new PIXI.Sprite(resources.player.texture);

    player.anchor.x = 0.5;
    player.anchor.y = 0.5;
    
    player.scale = new PIXI.Point(0.3,0.6);

    background.anchor.x = 0.5;
    background.anchor.y = 0.5;

    player.x = app.renderer.width/2;
    player.y = app.renderer.height*0.9;

    // Setup the position of the haruka
    background.x = app.renderer.width/2;
    background.y = app.renderer.height/2;

    // Add the haruka to the scene we are building
    app.stage.addChild(background);
    app.stage.addChild(player);

    var bg_speed = 1;
    var spawn_buffer = 0;
    var bullet_cooldown = 0;

    app.ticker.add(updateLevel);

    app.ticker.add(spawnEnemy);
    

    app.ticker.add(() => {
        keyboard.update();

        if (keyboard.isKeyDown("Space")){            
            spawnBullet()
            
        }

        if (keyboard.isKeyDown("ArrowUp")){
            if (player.y<=10) {
                player.y=player.y
            }else{
                player.y -=3
            }
        }else if (keyboard.isKeyDown("ArrowDown")) {
            if (player.y>=590) {
                player.y=player.y
            }else{
                player.y +=3
            }
        }
        
        if (keyboard.isKeyDown("ArrowLeft")){
            if (player.x<=(app.renderer.width/2)-195) {
                player.x=player.x
            }else{
                player.x -=5
            }
        }else if (keyboard.isKeyDown("ArrowRight")) {
            if (player.x>=(app.renderer.width/2)+195) {
                player.x=player.x
            }else{
                player.x +=5
            }
        }        
    });

    function spawnBullet() {        
        if (bullet_cooldown > 0) {
           return;
        }else{
             //enemies.push(createEnemy())
             let spritel = new PIXI.Sprite(app.loader.resources['laserino'].texture);
             let spriter = new PIXI.Sprite(app.loader.resources['laserino'].texture);
             spritel.anchor.set(0.5);
             spritel.scale.set(1.2)    
             spritel.position.set(player.position.x-10,player.position.y);
             spriter.anchor.set(0.5);
             spriter.scale.set(1.2)
             spriter.position.set(player.position.x+10,player.position.y);
             app.stage.addChild(spritel);
             app.stage.addChild(spriter);
             bullets.push(spritel)
             bullets.push(spriter)
             bullet_cooldown=50;
             //return sprite;
        }
    }

    function spawnEnemy(delta) {
        spawn_buffer += delta;
        if (spawn_buffer > 120/bg_speed) {
            //enemies.push(createEnemy())
            spawn_buffer=0;
        }
    }

    function updateLevel(delta) {
        background.tilePosition.y += 100*Math.log10(bg_speed);
        bg_speed+=0.0001;        
        tickEnemy(delta);
        updateBullets(delta);
    }

    function updateBullets(delta) {
        bullet_cooldown-=10;
        bullets.forEach(b => {
            b.position.y-=20
        });
    }

    function tickEnemy(delta) {
        enemies.forEach(item => {
            item.phase += delta * 0.1 * bg_speed;
            item.position.x = 400 + 150 * Math.cos(item.phase);        
            item.position.y += bg_speed;            
        });
        
    }



}




function createEnemy() {
    let obj = new PIXI.Sprite(app.loader.resources['enemy_a'].texture);
    obj.position.set(0,-100);
    obj.anchor.set(0.5);
    obj.scale.set(.5);
    app.stage.addChild(obj);
    obj.phase = 0;
    return obj;
}